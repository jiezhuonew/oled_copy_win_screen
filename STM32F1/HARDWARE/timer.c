#include "timer.h"
#include "usart3.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//
//在usart3_init()中已经被自动调用
//以下内容为USART3串口接收函数服务 记录是否超出最大等待时间 100ms
//于 USART3_IRQHandler 函数中收到串口数据使能 
//于 TIM7_IRQHandler   函数中超出设定最大等待时间记录一次串口接收结束
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////// 

extern u8 flag1s;

extern void KeyScan(void);

//定时器2中断服务程序		    
void TIM2_IRQHandler(void)
{ 	
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)	//是更新中断
	{	 			   
		USART3_RX_STA|=1<<15;							//标记接收完成
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update  );  	//清除TIM2更新中断标志    
		TIM_Cmd(TIM2, DISABLE);  						//关闭TIM3
	}
}
 
//通用定时器中断初始化
//这里始终选择为APB1的2倍，而APB1为36M
//arr：自动重装值。
//psc：时钟预分频数
void TIM2_Int_Init(u16 arr,u16 psc)
{	
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);		//TIM7时钟使能    
	
	//定时器TIM7初始化
	TIM_TimeBaseStructure.TIM_Period = arr; 					//设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
	TIM_TimeBaseStructure.TIM_Prescaler =psc; 					//设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 	//设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //TIM向上计数模式
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); 			//根据指定的参数初始化TIMx的时间基数单位
 
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE ); 					//使能指定的TIM7中断,允许更新中断

	 	  
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0 ;	//抢占优先级0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;			//子优先级1
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;				//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);								//根据指定的参数初始化VIC寄存器
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//
//以下内容主函数定时标志位设置所用 定时1ms  10-1 7200-1
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////// 

void TIM3_Int_Init(u16 arr,u16 psc)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);		//使能TIM3时钟
	
	TIM_TimeBaseInitStructure.TIM_Period		=	arr;	//初始化定时器3
	TIM_TimeBaseInitStructure.TIM_Prescaler		=	psc;
	TIM_TimeBaseInitStructure.TIM_CounterMode	=	TIM_CounterMode_Up;
	TIM_TimeBaseInitStructure.TIM_ClockDivision	=	TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStructure);
	
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);				//允许定时器3更新中断
	
	NVIC_InitStructure.NVIC_IRQChannel 						=	TIM3_IRQn;	//初始化NVIC
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority	=	0x01;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority			=	0x03;
	NVIC_InitStructure.NVIC_IRQChannelCmd 					=	ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	TIM_Cmd(TIM3,ENABLE);									//使能定时器3
}

void TIM3_IRQHandler(void)
{
	static u16 tmrms=0;
	
	if(TIM_GetITStatus(TIM3,TIM_IT_Update) == SET)
	{
		tmrms++;
		if(tmrms>=1000)
		{
			tmrms=0;
			flag1s=1;			//到达1s
		}
	}
	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);
}
	 
