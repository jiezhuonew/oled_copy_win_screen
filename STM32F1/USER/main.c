//////////////////////////////////////////////////////////////////////////////////
//请保留此信息以支持作者 谢谢													//
//作者:一只程序缘																//
//哔哩哔哩账号:237304109														//
//本视频链接:https://www.bilibili.com/video/BV1Kp4y1z7BV/						//
//学习交流QQ群:650846694														//
//最后优化时间:2020/11/27														//
//////////////////////////////////////////////////////////////////////////////////

//代码说明：此代码从串口3接收上位机发送来的数据并在oled显示图像 配合python代码一起使用
//主控：STM32F1

#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "timer.h"
#include "usart3.h"
#include "oled.h"

u8 flag1s;

int main(void)
{		
	int i=0;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	delay_init();
	uart_init(115200);
	usart3_init(115200);
	TIM3_Int_Init(10-1,7200-1);
	OLED_Init();
	
	while(1)
	{
		UART3_Driver();
		
		if(flag1s)
		{
			flag1s=0;
			
			i++;
			printf("%d\r\n",i);
		}
	}
} 

void ScanScreem(u8 *chr)
{
	u8 x,y;
	u16 i=0;
	
	for(y=0;y<8;y++)
	{
		OLED_Set_Pos(0,y);
		for(x=0;x<128;x++)
		{
			OLED_WR_Byte(chr[i++],OLED_DATA);
		}
	}
}


