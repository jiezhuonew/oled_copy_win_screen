#### 介绍
这是一个有关oled投屏的项目,大致工作流程是在电脑中运行python程序,程序实时对当前屏幕截图使用算法处理为黑白图像,通过串口发送数据至单片机接收,然后显示到oled上,单片机的代码提供了stm32f1和stm32f4的程序,通过这一个简单的例子让你了解电脑和单片机的一种交互方式

#### 使用说明
[单片机与电脑交互教程引入 python+opencv+仿badapple=投屏效果](https://www.bilibili.com/video/BV1Kp4y1z7BV)